
import matplotlib.pyplot as plot
import numpy as np

simul_time = 3600*2 # 1 hora 

Alts = np.zeros(simul_time)
Vels = np.zeros(simul_time)
Comb = np.zeros(simul_time)

Grav   = -9.8       # m/s2
Altin  = 6371000    # m
Velin  = 0          # m/s
dt     = 0.001      # s pas de temps
m_coet = 10000      # kg massa coet buit
m_comb = 80000      # kg massa de combustible  
Imp_max= 30         # m/s2 impuls maxim
eff_com= 0.003      # eficiencia combustible
alt_consigna= 4000000 
                    # volem posar el coet a 4000km
Maxalt = 0.0
Accact = Grav
Velact = Velin
Altact = Altin
Impuls = 0.0
n=0
m=0


def PlotAll():
    
    fig = plot.figure(figsize=(12, 7), dpi=80)
    ax1 = fig.add_subplot(121)
    ax1.set_title("Alçada")    
    ax1.set_xlabel('t')
    ax1.set_ylabel('m')
    ax1.grid('ON')
    ax1.plot([0,simul_time],[Altin,Altin],'k-', lw=2)
    ax1.plot([0,simul_time],[Altin+alt_consigna,Altin+alt_consigna],'k--', lw=2)
    ax1.plot(Alts, c='g',label='Alt')
    
    ax1 = fig.add_subplot (222)
    ax1.set_title("Velocitat")    
    ax1.set_xlabel('')
    ax1.set_ylabel('m/s')
    ax1.plot(Vels, c='b',label='Vel')
    
    ax1 = fig.add_subplot (224)
    ax1.set_title("Combustible")    
    ax1.set_xlabel('')
    ax1.set_ylabel('kg')
    ax1.plot(Comb, c='r',label='Comb')
    plot.show()                         #pinta grafica




#**********************************************************************************

def LlegeixSensors():       # Simula la lectura dels sensors. Podem afegir soroll 
                            # per fer realista.
    alt=Altact - Altin      # Sensor d'altitud
    vel=Velact              # Sensor de velocitat
    acc=Accact              # Acceleròmetre
    comb=m_comb             # Combustible
    return(alt,vel,acc,comb)


#**********************************************************************************

def Controla_Coet (t_act,consigna):  
                            # Aquest és el codi del micro que regeix el coet.
                            # Es crida cada 1ms    
    altitud, velocitat, accel, combustible = LlegeixSensors()
                            # Obtenim les dades dels sensors


    t = velocitat / -Grav
    h = altitud + velocitat * t
    # Calculem impuls: 2 valors (Max o 0)
    # Condició: v < 0 i encara no es troba a l'altitud consigna o ja està en marxa però l'altitud següent encara no sobrepassarà la consigna
    if t_act < 20*60*1000:
        impuls = Imp_max if (velocitat < 0 and altitud < alt_consigna) or (velocitat >= 0 and h <= alt_consigna) else 0
    else:
        impuls = 9 if (altitud < alt_consigna) else 0
    return(impuls)
#**********************************************************************************



# SIMULADOR 

while (n<simul_time*1000):              # Pas de simulació 1ms
    
    Impuls=Controla_Coet (n,alt_consigna)
 
    if(Altact<Altin): 
        print("CRASH")                  # Cau a terra
        break
    if(m_comb<=0):                      # No pot haver impuls sense fuel
        Impuls=0
        m_comb=0
    m_comb = m_comb - eff_com * Impuls  # Actualitzem massa combustible
    Mtotal = m_comb + m_coet            # Actualitzem massa total
    Accact = Grav + Impuls              # Actualitzem acceleració
    Velact = Velact + Accact*dt         # actialitzem velocitat
    Altact = Altact + Velact*dt         # Actualitzem altitud actual
    if (Altact>Maxalt):
        Maxalt=Altact                   # Registren alçada màxima   
    if(n%1000==0):                      # Cada un segon captem dades pel gràfic
        Alts[m]= Altact
        Vels[m]= Velact
        Comb[m]= m_comb
        m=m+1
    n=n+1
    
print("REPORT:")
print("Max alt=",Maxalt-Altin,"Fuel restant=",m_comb)
PlotAll()


    
    
    

