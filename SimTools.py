# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 09:52:22 2020

@author: enric
"""
import matplotlib.pyplot as plt
import numpy as np


class Bloc_RC():
        
    def __init__(self,Rin,Cap,Vini,Qini):
    
        self.R      = Rin
        self.C      = Cap
        self.Vcap   = Vini
        self.Q      = Qini
        self.ctemps = 0
        self.I      = 0
        self.bt     = 0
      
    def actualitza_RC(self,dt,Vin):
        
        self.ctemps=self.ctemps+dt
        self.Vcap=self.Q*self.C
        self.I=(Vin-self.Vcap)/self.R
        self.Q=self.Q+self.I*dt*1000000
        return(self.Vcap,self.Q,self.I)

    
    
class Font_V():    
    
    def __init__(self,forma,vmin,vmax,ft):
    
        self.tfont  = 0
        self.func   = forma
        self.vmin   = vmin
        self.vmax   = vmax
        self.ft     = ft
        self.state  = 0
        
    def genera(self,dt):
        
        self.tfont=self.tfont+dt
        
        if self.func=='quadrat':
            if self.tfont%(500000/self.ft)==0:
                if self.state==0: 
                    self.state=1
                else:
                    self.state=0
            if self.state==0: 
                return self.vmin
            else:
                return self.vmax

        if self.func=='grao':
            if self.tfont<self.ft:            
                return self.vmin
            else:
                return self.vmax
                
        if self.func=='sinus':
            return (self.vmin+self.vmax)/2.0+(self.vmax-self.vmin)/2.0*np.sin((2*3.141592654*0.000001*self.ft)*self.tfont)
       
        
   
class Simulador():
    
    def __init__(self,Temps,Tr,dims):
        
        self.Temps = Temps
        self.Tr    = 1
        self.t1    = np.zeros(Temps)
        self.t2    = np.zeros(Temps)
        self.t3    = np.zeros(Temps)
        self.t4    = np.zeros(Temps)    
    
    def afegir(self,t,v1,v2,v3,v4):
        
        self.t1[t] = v1
        self.t2[t] = v2
        self.t3[t] = v3
        self.t4[t] = v4
        
    def pinta(self,b1,b2,b3,b4):

        if b1:
            plt.plot(self.t1)
        if b2:
            plt.plot(self.t2)
        if b3:
            plt.plot(self.t3)
        if b4:
            plt.plot(self.t4)        
        plt.ylim(-1,6)
        plt.ylabel('Vcap (Volts)')
        plt.xlabel('Temps (us)')
        plt.show()
        
    
    
