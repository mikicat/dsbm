# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 22:21:20 2020
@author: enric
"""

import SimTools as CT
import sys
if len(sys.argv) != 3: print("SimuladorCap.py [Freq] [Forma: {quadrat/grao/sinus}]"); exit()
Temps    = 200000                                  # Duració us experiment
Ffont    = int(sys.argv[1])                        # Frequencia font Hz


Font  = CT.Font_V    (sys.argv[2],0,5,Ffont)         # forma,Vmin,Vmax,freq Hz
RC1   = CT.Bloc_RC   (10000,1E-6,0,0)              # R ohm,Cfarad,Vini,Qini      
Sim   = CT.Simulador (Temps,1,2)                   # t simul,pas us, #canals

t=0
while (t<Temps):         
    
    Vf       = Font.genera(1)     
    Vc,Qc,Ic = RC1.actualitza_RC(1,Vf) 
    Sim.afegir(t,Vf,Vc,0,0)
    t=t+1
    
Sim.pinta(1,1,0,0)

    
    

