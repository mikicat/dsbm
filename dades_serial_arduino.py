
# Codi Python al PC

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import serial
import csv


def animate(i):
    try:
        y[i]=int(Port.readline().split(b'\r\n')[0].decode("ascii"))
        print(str(Port.readline().split(b'\r\n')[0]))

        line.set_ydata(y)  # update the data
        
        writer.writerow([y[i]])
        count += 1
        if count > 10000:
            f.close()
            exit()
    except:
        pass
    return line,

#Init only required for blitting to give a clean slate.
def init():
    line.set_ydata(y)
    return line,

f = open('llumdsbm.csv', 'w+')

writer = csv.writer(f)
writer.writerow(["Valor Llum"])

count = 0

fig = plt.figure()
ax = fig.add_subplot(111)

x=y=  list(range(0,100,1)) # x,y-array
line, = ax.plot(x,y)
Port = serial.Serial('/dev/ttyACM0', 115200)
ani = animation.FuncAnimation(fig, animate, range(0,100), init_func=init,
    interval=25, blit=False)
plt.axis([0,100,0,1024])

plt.show()


 

